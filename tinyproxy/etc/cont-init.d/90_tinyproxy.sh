#!/bin/ash
set -e

TP_CONF="/etc/tinyproxy/tinyproxy.conf"

: ${LISTEN_PORT:=8888}
: ${_USER:="tinyproxy"}
: ${_GROUP:="tinyproxy"}
: ${ALLOWED:="127.0.0.1"}
: ${CONNECT_PORTS:="443 563"}
: ${LOG_TO_SYSLOG:="On"}
: ${LOGLEVEL:="Info"}
: ${MAXCLIENTS:="100"}
: ${MINSPARESERVERS:="5"}
: ${MAXSPARESERVERS:="20"}
: ${STARTSERVERS:="10"}
: ${XTINYPROXY:="No"}

if [[ ! -f $TP_CONF ]]
 then
	cat > $TP_CONF <<EOF
User $_USER
Group $_GROUP
Port $LISTEN_PORT
Syslog $LOG_TO_SYSLOG
LogLevel $LOGLEVEL
MaxClients $MAXCLIENTS
MinSpareServers $MINSPARESERVERS
MaxSpareServers $MAXSPARESERVERS
StartServers $STARTSERVERS
PidFile "/tmp/tinyproxy.pid"
XTinyproxy $XTINYPROXY
DisableViaHeader On
Anonymous "Authorization"
MaxRequestsPerChild 0
StatHost "tinyproxy.stats"
StatFile "/usr/share/tinyproxy/stats.html"
Timeout 600
EOF

for a in $ALLOWED
 do
	echo "Allow $a" >> $TP_CONF
done

for p in $CONNECT_PORTS
 do
	echo "ConnectPort $p" >> $TP_CONF
done
fi
